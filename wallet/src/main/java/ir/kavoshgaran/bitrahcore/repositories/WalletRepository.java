package ir.kavoshgaran.bitrahcore.repositories;

import ir.kavoshgaran.bitrahcore.entities.Coin;
import ir.kavoshgaran.bitrahcore.entities.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {

    Optional<Wallet> findByCoinAndAddress(Coin coin, String address);
}
