package ir.kavoshgaran.bitrahcore.repositories;

import ir.kavoshgaran.bitrahcore.entities.CurrencyPrice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyPriceRepository extends JpaRepository<CurrencyPrice, Long> {
    CurrencyPrice findByIso(String iso);
}
