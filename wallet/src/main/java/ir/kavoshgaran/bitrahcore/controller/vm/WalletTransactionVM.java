package ir.kavoshgaran.bitrahcore.controller.vm;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class WalletTransactionVM implements Serializable {

    private String transactionId;
}
