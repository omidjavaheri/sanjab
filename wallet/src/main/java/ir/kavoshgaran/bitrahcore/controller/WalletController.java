package ir.kavoshgaran.bitrahcore.controller;

import ir.kavoshgaran.bitrahcore.controller.vm.WalletVM;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.transactions.services.AccountStatus;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import ir.kavoshgaran.bitrahcore.entities.WalletResponseDTO;
import ir.kavoshgaran.bitrahcore.services.WalletService;
import ir.kavoshgaran.bitrahcore.services.dto.WalletBalanceDTO;
import ir.kavoshgaran.bitrahcore.utils.WalletResponseUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/v1/wallet/{coin}")
public class WalletController extends WalletBaseController {

    private final WalletService walletService;

    public WalletController(WalletService walletService) {
        this.walletService = walletService;
    }

    @GetMapping("/generate")
    public ResponseEntity<WalletResponseDTO<WalletVM>> generateWallet(@PathVariable("coin") String coinIso) {
        Coin byIso = Coin.findByIso(coinIso);
        String generatedWallet = this.walletService.generateWallet(byIso);
        return ResponseEntity.ok(
                WalletResponseUtils.response(new WalletVM(generatedWallet)));
    }

    @GetMapping("/balance/{address}")
    public ResponseEntity<WalletResponseDTO<WalletBalanceDTO>> checkBalance(@PathVariable("coin") String coinIso, @PathVariable("address") String address) {
        Coin byIso = Coin.findByIso(coinIso);
        WalletBalanceDTO walletBalanceDTO = this.walletService.checkBalance(byIso, address);
        return ResponseEntity.ok(
                WalletResponseUtils.response(walletBalanceDTO));
    }

    @GetMapping("/balance/final/{address}")
    public ResponseEntity<WalletResponseDTO<AccountStatus>> checkFinalBalance(@PathVariable("coin") String coinIso, @PathVariable("address") String address) {
        Coin byIso = Coin.findByIso(coinIso);
        AccountStatus walletBalanceDTO = this.walletService.checkFinalBalance(byIso, address);
        return ResponseEntity.ok(
                WalletResponseUtils.response(walletBalanceDTO));
    }

    @GetMapping("/balance/final/{address}/map")
    public ResponseEntity<WalletResponseDTO<Map<CryptoCurrencyApiProvider, AccountStatus>>> checkFinalBalanceMap(@PathVariable("coin") String coinIso, @PathVariable("address") String address) {
        Coin byIso = Coin.findByIso(coinIso);
        Map<CryptoCurrencyApiProvider, AccountStatus>
                cryptoCurrencyApiProviderCryptoCurrencyTransactionStatusMap = this.walletService.checkFinalBalanceMap(byIso, address);
        return ResponseEntity.ok(
                WalletResponseUtils.response(cryptoCurrencyApiProviderCryptoCurrencyTransactionStatusMap));
    }

}
