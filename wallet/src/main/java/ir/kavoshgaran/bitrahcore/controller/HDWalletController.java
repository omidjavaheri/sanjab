package ir.kavoshgaran.bitrahcore.controller;

import ir.kavoshgaran.bitrahcore.controller.vm.HDWalletVM;
import ir.kavoshgaran.bitrahcore.core.hdwallets.HDWallet;
import ir.kavoshgaran.bitrahcore.core.hdwallets.HDWalletCredentials;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import ir.kavoshgaran.bitrahcore.entities.WalletResponseDTO;
import ir.kavoshgaran.bitrahcore.services.WalletService;
import ir.kavoshgaran.bitrahcore.services.dto.WalletBalanceDTO;
import ir.kavoshgaran.bitrahcore.utils.WalletResponseUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/v1/hd-wallet/{coin}")
public class HDWalletController extends WalletBaseController {

    private final WalletService walletService;

    public HDWalletController(WalletService walletService) {
        this.walletService = walletService;
    }

    @GetMapping("/mnemonic")
    public ResponseEntity<WalletResponseDTO<HDWalletVM>> generateMnemonic(Authentication authentication) {
        final Object credentials = authentication.getCredentials();
        String mnemonic = this.walletService.generateMnemonic();
        return ResponseEntity.ok(
                WalletResponseUtils.response(new HDWalletVM()
                .setMnemonic(mnemonic)));
    }

    @PostMapping("/generate")
    public ResponseEntity<WalletResponseDTO<HDWalletVM>> generateWallet(@PathVariable("coin") String coinIso, @RequestBody HDWalletCredentials credentials) {
        Coin byIso = Coin.findByIso(coinIso);
        HDWallet hdWallet = this.walletService.generateHDWallet(byIso, credentials.getPassword());
        return ResponseEntity.ok(
                WalletResponseUtils.response(new HDWalletVM()
                .setMnemonic(hdWallet.getCredentials().getMnemonic())
                .setAddress(hdWallet.getHdWalletAddress().getAddress())
                .setPublicKey(hdWallet.getHdWalletAddress().getPublicKey())
                .setXPublicKey(hdWallet.getHdWalletAddress().getXPublicKey())));
    }

    @PostMapping("/retrieve")
    public ResponseEntity<WalletResponseDTO<HDWalletVM>> retrieveWallet(@PathVariable("coin") String coinIso, @RequestBody HDWalletCredentials credentials) {
        Coin byIso = Coin.findByIso(coinIso);
        HDWallet hdWallet = this.walletService.retrieveHDWallet(byIso, credentials.getMnemonic(), credentials.getPassword());
        return ResponseEntity.ok(
                WalletResponseUtils.response(new HDWalletVM()
                .setMnemonic(hdWallet.getCredentials().getMnemonic())
                .setAddress(hdWallet.getHdWalletAddress().getAddress())
                .setPublicKey(hdWallet.getHdWalletAddress().getPublicKey())
                .setXPublicKey(hdWallet.getHdWalletAddress().getXPublicKey())));
    }

    @PostMapping("/balance")
    public ResponseEntity<WalletResponseDTO<WalletBalanceDTO>> checkBalance(@PathVariable("coin") String coinIso, @RequestBody HDWalletCredentials credentials) {
        Coin byIso = Coin.findByIso(coinIso);
        String address = this.walletService.retrieveHDWallet(byIso, credentials.getMnemonic(), credentials.getPassword()).getHdWalletAddress().getAddress();
        WalletBalanceDTO walletBalanceDTO = this.walletService.checkBalance(byIso, address);
        return ResponseEntity.ok(
                WalletResponseUtils.response(walletBalanceDTO));
    }

}
