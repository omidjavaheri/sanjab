package ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoinsv;

import ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoin.BitcoinUtils;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericForkTransaction;

import java.math.BigDecimal;

public class BitcoinSVTransaction extends GenericForkTransaction {
    public BigDecimal convertValue(BigDecimal value) {
        return BitcoinUtils.bitcoinToSatoshi(value);
    }
}
