package ir.kavoshgaran.bitrahcore.core.transactions.excoino;

import ir.kavoshgaran.bitrahcore.entities.Coin;
import ir.kavoshgaran.bitrahcore.services.CurrencyPriceService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;

@Component
public class ExcoinoServices {

    private final CurrencyPriceService currencyPriceService;

    public ExcoinoServices(CurrencyPriceService currencyPriceService) {
        this.currencyPriceService = currencyPriceService;
    }

    public ExcoinoResult convertCurrencies(String fromIso, String toIso, BigDecimal value, boolean isMerchant) {
        final BigDecimal[] fromFiatRatio = new BigDecimal[1];
        final BigDecimal[] toFiatRatio = new BigDecimal[1];
        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl
                = "https://www.excoino.com/api/v3/bitrahcore/currencies";
        ResponseEntity<List> response
                = restTemplate.getForEntity(fooResourceUrl, List.class);
        response.getBody().forEach(r -> {
            HashMap<String, String> res = ((HashMap) r);
            if (res.get("iso").equals(fromIso)) {
                fromFiatRatio[0] = new BigDecimal(String.valueOf(isMerchant ? res.get("buy_price") : res.get("sell_price")));
            }
            if (res.get("iso").equals(toIso)) {
                toFiatRatio[0] = new BigDecimal(String.valueOf(isMerchant ? res.get("buy_price") : res.get("sell_price")));
            }
        });
        if (toFiatRatio[0] == null) toFiatRatio[0] = this.currencyPriceService.getPrice(toIso);
        BigDecimal result = value.divide(toFiatRatio[0].divide(fromFiatRatio[0], 20, RoundingMode.HALF_UP), 20, RoundingMode.HALF_UP).setScale(Coin.findByIso(toIso).getPrecision(), RoundingMode.HALF_UP);
        return new ExcoinoResult(fromFiatRatio[0], toFiatRatio[0], result);
    }
//
//    public static void main(String[] args) {
//        ExcoinoServices.convertCurrencies("IRR", "BTC", new BigDecimal("150000"), true);
//    }

}
