package ir.kavoshgaran.bitrahcore.core.providers.btcexplorer;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiServiceAdapter;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(6)
public class BtcExplorerService extends CryptoCurrencyApiServiceAdapter {

    @Override
    public TransactionResult pushTransaction(Coin coin, String transactionHex) {
        throw new CryptoCurrencyApiException(CryptoCurrencyError.API_DOES_NOT_SUPPORT_PUSH_TX);
    }

    @Override
    public CryptoCurrencyApiProvider getApiProvider() {
        return CryptoCurrencyApiProvider.BTC_EXPLORER;
    }

}
