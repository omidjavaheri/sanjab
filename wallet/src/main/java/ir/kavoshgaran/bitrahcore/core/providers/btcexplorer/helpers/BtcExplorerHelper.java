package ir.kavoshgaran.bitrahcore.core.providers.btcexplorer.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.ApiHelper;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;

public interface BtcExplorerHelper extends ApiHelper {

    @Override
    default CryptoCurrencyApiProvider getProvider() {
        return CryptoCurrencyApiProvider.BTC_EXPLORER;
    }
}
