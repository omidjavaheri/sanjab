package ir.kavoshgaran.bitrahcore.core.providers.tokenview.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.tokenview.api.TokenViewClient;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericUTXO;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TokenViewDogecoinHelper extends TokenViewGenericHelper {

    private static final Coin COIN = Coin.DOGECOIN;

    public TokenViewDogecoinHelper(TokenViewClient tokenViewClient) {
        super(tokenViewClient);
    }

    @Override
    public List<GenericUTXO> getUTXOs(String address) {
        return super.getGeneratedUTXOs(address);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
