package ir.kavoshgaran.bitrahcore.core.providers.blockchair.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.ApiHelper;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;

public interface BlockChairHelper extends ApiHelper {

    @Override
    default CryptoCurrencyApiProvider getProvider() {
        return CryptoCurrencyApiProvider.BLOCK_CHAIR;
    }
}
