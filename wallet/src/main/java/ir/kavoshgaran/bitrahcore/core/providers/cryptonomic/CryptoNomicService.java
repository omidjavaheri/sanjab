package ir.kavoshgaran.bitrahcore.core.providers.cryptonomic;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiServiceAdapter;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(6)
public class CryptoNomicService extends CryptoCurrencyApiServiceAdapter {

    @Override
    public CryptoCurrencyApiProvider getApiProvider() {
        return CryptoCurrencyApiProvider.CRYPTO_NOMIC;
    }

    @Override
    public TransactionResult pushTransaction(Coin coin, String transactionHex) {
        return null;
    }
}
