package ir.kavoshgaran.bitrahcore.core.addresses;

import ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoin.BitcoinUtils;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoincash.BitcoinCashUtils;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoinsv.BitcoinSVUtils;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.dash.DashUtils;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.dogecoin.DogecoinUtils;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.ethereum.EthereumUtils;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.litecoin.LitecoinUtils;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoSdkService;
import ir.kavoshgaran.bitrahcore.entities.Coin;

public class AddressValidatorFactory {

    public static void validateAddress(Coin coin, String address) {
        switch (coin) {
            case BITCOIN:
                if (BitcoinUtils.isAddressInvalid(address))
                    throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
                break;
            case LITECOIN:
                if (LitecoinUtils.isAddressInvalid(address))
                    throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
                break;
            case DOGECOIN:
                if (DogecoinUtils.isAddressInvalid(address))
                    throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
                break;
            case BITCOINCASH:
                if (BitcoinCashUtils.isAddressInvalid(address))
                    throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
                break;
            case BITCOINSV:
                if (BitcoinSVUtils.isAddressInvalid(address))
                    throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
                break;
            case DASH:
                if (DashUtils.isAddressInvalid(address))
                    throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
                break;
            case ETHEREUM:
            case TETHER:
                if (!EthereumUtils.isAddressValid(address))
                    throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
                break;
            default:
                if (!CryptoSdkService.validateAddress(coin, address))
                    throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
        }
    }
}
