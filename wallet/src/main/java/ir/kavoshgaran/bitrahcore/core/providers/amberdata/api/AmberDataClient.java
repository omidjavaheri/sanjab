package ir.kavoshgaran.bitrahcore.core.providers.amberdata.api;

import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.models.AmberDataAccountTransactions;
import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.models.AmberDataAddressInfo;
import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.models.AmberDataToken;
import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.models.AmberDataTransaction;
import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.models.request.AmberDataPushTransactionRequest;
import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.models.response.*;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;

import static ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.AmberDataConstants.DEFAULT_JSON_RPC;
import static ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.AmberDataConstants.DEFAULT_PUSH_ID;


@Component
public class AmberDataClient {

    private final WebClient webClient;
    //    @Value("${bitrah.crypto.api.amberdata.apiKey}")
    private String AMBER_DATA_API_KEY = "UAK4434a0ef0f903728313d7b8e47ca1467";

    public AmberDataClient(WebClient webClient) {
        this.webClient = webClient;
    }

    private String getBlockChainId(Coin coin) {
        switch (coin) {
            case ETHEREUM:
            case TETHER:
                return "ethereum-mainnet";
            case BITCOIN:
                return "bitcoin-mainnet";
            case BITCOINSV:
                return "bitcoin-sv-mainnet";
            case LITECOIN:
                return "litecoin-mainnet";
            case ZCASH:
                return "zcash-mainnet";
            default:
                throw new CryptoCurrencyApiException(CryptoCurrencyError.COIN_NOT_SUPPORTED);
        }
    }

    private String getJsonRPCBlockChainId(Coin coin) {
        switch (coin) {
            case ETHEREUM:
            case TETHER:
                return "1c9c969065fcd1cf";
            case BITCOIN:
                return "408fa195a34b533de9ad9889f076045e";
            default:
                throw new CryptoCurrencyApiException(CryptoCurrencyError.COIN_NOT_SUPPORTED);
        }
    }

    public AmberDataAddressInfo getAddress(Coin coin, String address) {

        String blockChainId = getBlockChainId(coin);
        String uri = AmberDataConstants.getAddressInfoURL(address);

        Mono<AmberDataAddressResponse> bodyMono = webClient.get()
                .uri(uri)
                .header(AmberDataConstants.HEADER_TOKEN_KEY, AMBER_DATA_API_KEY)
                .header(AmberDataConstants.HEADER_BLOCKCHAIN_ID_KEY, blockChainId)
                .retrieve().bodyToMono(AmberDataAddressResponse.class);

        return bodyMono.block().getPayload();
    }


    public AmberDataTransaction getTransaction(Coin coin, String transactionHash) {
        String blockChainId = getBlockChainId(coin);
        String uri = AmberDataConstants.getTransactionsURL(transactionHash);

        Mono<AmberDataTransactionResponse> bodyMono = webClient.get()
                .uri(uri)
                .header(AmberDataConstants.HEADER_TOKEN_KEY, AMBER_DATA_API_KEY)
                .header(AmberDataConstants.HEADER_BLOCKCHAIN_ID_KEY, blockChainId)
                .retrieve().bodyToMono(AmberDataTransactionResponse.class);

        return bodyMono.block().getPayload();
    }

    public String pushTransaction(Coin coin, String transactionHex) {
        String uri = AmberDataConstants.getPushTransactionURL(AMBER_DATA_API_KEY);

        AmberDataPushTransactionRequest pushTransactionRequest = new AmberDataPushTransactionRequest();
        pushTransactionRequest.setJsonrpc(DEFAULT_JSON_RPC);
        pushTransactionRequest.setId(DEFAULT_PUSH_ID);
        pushTransactionRequest.setMethod(AmberDataConstants.getJsonRPCPushMethod(coin));
        pushTransactionRequest.setParams(Collections.singletonList(transactionHex));

        Mono<AmberDataPushTransactionResponse> bodyMono = webClient.post()
                .uri(uri)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AmberDataConstants.HEADER_BLOCKCHAIN_ID_KEY, this.getJsonRPCBlockChainId(coin))
                .body(BodyInserters.fromValue(pushTransactionRequest))
                .retrieve().bodyToMono(AmberDataPushTransactionResponse.class);

        AmberDataPushTransactionResponse response = bodyMono.block();

        return response.getResult();
    }

    public AmberDataAccountTransactions getTransactionOfAddress(Coin coin, String address) {
        String blockChainId = getBlockChainId(coin);
        String uri = AmberDataConstants.getTransactionsOfAddressURL(address);

        Mono<AmberDataAccountTransactionsResponse> bodyMono = webClient.get()
                .uri(uri)
                .header(AmberDataConstants.HEADER_TOKEN_KEY, AMBER_DATA_API_KEY)
                .header(AmberDataConstants.HEADER_BLOCKCHAIN_ID_KEY, blockChainId)
                .retrieve().bodyToMono(AmberDataAccountTransactionsResponse.class);

        return bodyMono.block().getPayload();
    }

    public AmberDataAccountTransactions getPendingTransactionsOfAddress(Coin coin, String address) {
        String blockChainId = getBlockChainId(coin);
        String uri = AmberDataConstants.getPendingTransactionsOfAddressURL(address);

        Mono<AmberDataAccountTransactionsResponse> bodyMono = webClient.get()
                .uri(uri)
                .header(AmberDataConstants.HEADER_TOKEN_KEY, AMBER_DATA_API_KEY)
                .header(AmberDataConstants.HEADER_BLOCKCHAIN_ID_KEY, blockChainId)
                .retrieve().bodyToMono(AmberDataAccountTransactionsResponse.class);

        return bodyMono.block().getPayload();
    }

    public AmberDataAccountTransactions getPendingTransactionOfEthereumAddress(Coin coin, String address) {
        String blockChainId = getBlockChainId(coin);
        String uri = AmberDataConstants.getPendingTransactionsOfAddressURL(address);

        Mono<AmberDataAccountTransactionsResponse> bodyMono = webClient.get()
                .uri(uri)
                .header(AmberDataConstants.HEADER_TOKEN_KEY, AMBER_DATA_API_KEY)
                .header(AmberDataConstants.HEADER_BLOCKCHAIN_ID_KEY, blockChainId)
                .retrieve().bodyToMono(AmberDataAccountTransactionsResponse.class);

        return bodyMono.block().getPayload();
    }

    public List<AmberDataToken> getTokensOfAddress(Coin coin, String address) {
        String blockChainId = getBlockChainId(coin);
        String uri = AmberDataConstants.getTokensURL(address);

        Mono<AmberDataTokensResponse> bodyMono = webClient.get()
                .uri(uri)
                .header(AmberDataConstants.HEADER_TOKEN_KEY, AMBER_DATA_API_KEY)
                .header(AmberDataConstants.HEADER_BLOCKCHAIN_ID_KEY, blockChainId)
                .retrieve().bodyToMono(AmberDataTokensResponse.class);

        return bodyMono.block().getPayload().getRecords();
    }


}
