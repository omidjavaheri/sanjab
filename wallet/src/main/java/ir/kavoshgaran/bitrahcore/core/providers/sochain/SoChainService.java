package ir.kavoshgaran.bitrahcore.core.providers.sochain;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiServiceAdapter;
import ir.kavoshgaran.bitrahcore.core.providers.sochain.api.SoChainClient;
import ir.kavoshgaran.bitrahcore.core.providers.sochain.api.model.response.SoChainPushTransactionResponse;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResultPayload;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Service;

@Service
public class SoChainService extends CryptoCurrencyApiServiceAdapter {

    private final SoChainClient soChainClient;

    public SoChainService(SoChainClient soChainClient) {
        this.soChainClient = soChainClient;
    }

    @Override
    public CryptoCurrencyApiProvider getApiProvider() {
        return CryptoCurrencyApiProvider.SO_CHAIN;
    }

    @Override
    public TransactionResult pushTransaction(Coin coin, String transactionHex) {
        SoChainPushTransactionResponse pushTransactionResponse = this.soChainClient.pushTransaction(coin, transactionHex);
        TransactionResult transactionResult = new TransactionResult();
        TransactionResultPayload transactionResultPayload = new TransactionResultPayload();
        transactionResultPayload.setTxid(pushTransactionResponse.getTransactionId());
        transactionResult.setPayload(transactionResultPayload);
        transactionResult.setProvider(CryptoCurrencyApiProvider.SO_CHAIN);
        return transactionResult;
    }
}
