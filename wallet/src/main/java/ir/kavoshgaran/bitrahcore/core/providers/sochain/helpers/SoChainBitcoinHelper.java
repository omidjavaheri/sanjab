package ir.kavoshgaran.bitrahcore.core.providers.sochain.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.sochain.api.SoChainClient;
import ir.kavoshgaran.bitrahcore.core.transactions.CryptoCurrencyPayment;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericTransaction;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericTransactionBuilder;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericUTXO;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class SoChainBitcoinHelper extends SoChainGenericHelper {

    public SoChainBitcoinHelper(SoChainClient soChainClient) {
        super(soChainClient);
    }

    @Override
    public Coin getCoin() {
        return Coin.BITCOIN;
    }

}
