package ir.kavoshgaran.bitrahcore.core.addresses.tron;

import ir.kavoshgaran.bitrahcore.core.addresses.GeneratedAddress;
import ir.kavoshgaran.bitrahcore.utils.Base58Utils;
import ir.kavoshgaran.bitrahcore.utils.BitrahHashUtils;
import ir.kavoshgaran.bitrahcore.utils.BitrahHexUtils;
import ir.kavoshgaran.bitrahcore.utils.signature.ECKeyPair;
import ir.kavoshgaran.bitrahcore.utils.signature.ECSignUtils;
import org.bouncycastle.jcajce.provider.digest.Keccak;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomTronAddressGenerator {

    private final Logger logger = LoggerFactory.getLogger(CustomTronAddressGenerator.class);

    private static final int PUBLIC_KEY_LENGTH = 64;

    public static void main(String[] args) {
        new CustomTronAddressGenerator().generate();
    }

    private byte[] generatePublicKey(ECKeyPair keyPair) {
        Keccak.Digest256 digest256 = new Keccak.Digest256();
        byte[] publicKey = new byte[PUBLIC_KEY_LENGTH];
        System.arraycopy(keyPair.getPublicKey(), 1, publicKey, 0, PUBLIC_KEY_LENGTH);
//        System.arraycopy(BitrahHexUtils.decodeHexString(ECSignUtils.publicKeyFromPrivate(new BigInteger(BitrahHexUtils.decodeHexString("7a399a8190c4550819813d7b0ba0deee66bb877c62335a82953cac80925d7ec3"))).toString(16)), 0, publicKey, 0, PUBLIC_KEY_LENGTH);
        return digest256.digest(publicKey);
    }

    private String generateChecksum(String address) {
        String doubleHash = BitrahHashUtils.doubleSha256(address);
        return doubleHash.substring(0, 8);
    }

    private String generateTronAddress(ECKeyPair keyPair) {
        byte[] publicKey = this.generatePublicKey(keyPair);
        String sha3_256PublicKeyHex = BitrahHexUtils.bytesToHex(publicKey);
        String extractedAddress = "41" +
                sha3_256PublicKeyHex.substring(sha3_256PublicKeyHex.length() - 40);
        String checksum = this.generateChecksum(extractedAddress);
        String finalAddress = extractedAddress + checksum;
        return Base58Utils.encode(BitrahHexUtils.decodeHexString(finalAddress));
    }

    public GeneratedAddress generate() {
        ECKeyPair keyPair = ECSignUtils.generateECKeyPair();
        String address = this.generateTronAddress(keyPair);
        logger.debug(String.format("Tron Address Generator -> PrivateKey: %s || Address: %S", BitrahHexUtils.bytesToHex(keyPair.getPrivateKey()), address));
        return new GeneratedAddress(keyPair.getPrivateKeyHex(), address);
    }
}
