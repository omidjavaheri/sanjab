package ir.kavoshgaran.bitrahcore.core.providers.zcha.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.ApiHelper;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;

public interface ZchaHelper extends ApiHelper {

    @Override
    default CryptoCurrencyApiProvider getProvider() {
        return CryptoCurrencyApiProvider.ZCHA;
    }
}
