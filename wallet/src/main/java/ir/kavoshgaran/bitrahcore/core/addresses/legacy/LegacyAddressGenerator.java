package ir.kavoshgaran.bitrahcore.core.addresses.legacy;

import ir.kavoshgaran.bitrahcore.core.addresses.AddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.GeneratedAddress;
import ir.kavoshgaran.bitrahcore.utils.Base58Utils;
import ir.kavoshgaran.bitrahcore.utils.BitrahByteUtils;
import ir.kavoshgaran.bitrahcore.utils.BitrahHashUtils;
import ir.kavoshgaran.bitrahcore.utils.signature.ECKeyPair;
import ir.kavoshgaran.bitrahcore.utils.signature.ECSignUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public abstract class LegacyAddressGenerator implements AddressGenerator {

    public static byte[] publicKeyToAddress(byte[] pubKey, LegacyAddressVariant variant) throws NoSuchAlgorithmException {
        byte[] sha256Ripemd160Hashed = BitrahHashUtils.sha256Ripemd160(pubKey);
        byte[] versionBytes = variant.hasTwoVersionBytes()
                ? new byte[]{variant.getVersion(), variant.getSecondVersionByte()}
                : new byte[]{variant.getVersion()};

        byte[] versionPrepended = BitrahByteUtils.concatenate(versionBytes, sha256Ripemd160Hashed);
        byte[] doubleHashedToCalculateChecksum = BitrahHashUtils.doubleSha256Bytes(versionPrepended);
        return BitrahByteUtils.concatenate(versionPrepended, BitrahByteUtils.copyOfRange(doubleHashedToCalculateChecksum, 0, 4));
    }

    public static byte[] extractHash160FromAddress(String address) {
        byte[] decodedBytes = Base58Utils.decode(address);
        // Todo: ad-hoc implementation for ZCASH versionBytes
        int versionBytesLength = address.startsWith("t1") || address.startsWith("t2") || address.startsWith("t3") ? 2 : 1;
        return Arrays.copyOfRange(decodedBytes, versionBytesLength, decodedBytes.length - 4);
    }

    public GeneratedAddress generate() throws NoSuchAlgorithmException {
        ECKeyPair keyPair = ECSignUtils.generateECKeyPair();
        String privKey = WifGenerator.generateWif(keyPair.getPrivateKey(), getVariant());
        byte[] a1 = publicKeyToAddress(keyPair.getPublicKey(), getVariant());
        String address = Base58Utils.encode(a1);
        System.out.println(privKey);
        System.out.println(address);
        return new GeneratedAddress(privKey, address);
    }

    public boolean validateAddress(String address) {
        byte[] decodedBytes;

        try {
            decodedBytes = Base58Utils.decode(address);
        } catch (Exception e) {
            return false;
        }

        byte[] hash160Bytes = Arrays.copyOfRange(decodedBytes, 0, decodedBytes.length - 4);
        if (decodedBytes[0] != getVariant().getVersion()) return false;


        int versionByteLength = 1;
        if (getVariant().hasTwoVersionBytes())
            versionByteLength++;

        if (hash160Bytes.length != 20 + versionByteLength) return false;

        byte[] finalEncodedHash = BitrahHashUtils.doubleSha256Bytes(hash160Bytes);
        for (int i = 0; i < 4; i++) {
            if (decodedBytes[20 + versionByteLength + i] != finalEncodedHash[i]) return false;
        }

        return true;
    }

    public abstract LegacyAddressVariant getVariant();
}
