package ir.kavoshgaran.bitrahcore.core.providers.whatsonchain;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiServiceAdapter;
import ir.kavoshgaran.bitrahcore.core.providers.whatsonchain.api.WhatsOnChainClient;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResultPayload;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Service;

@Service
public class WhatsOnChainService extends CryptoCurrencyApiServiceAdapter {

    private final WhatsOnChainClient whatsOnChainClient;

    public WhatsOnChainService(WhatsOnChainClient whatsOnChainClient) {
        this.whatsOnChainClient = whatsOnChainClient;
    }

    @Override
    public CryptoCurrencyApiProvider getApiProvider() {
        return CryptoCurrencyApiProvider.WHATS_ON_CHAIN;
    }

    @Override
    public TransactionResult pushTransaction(Coin coin, String transactionHex) {
        String transactionId = this.whatsOnChainClient.pushTransaction(transactionHex);
        TransactionResult transactionResult = new TransactionResult();
        TransactionResultPayload transactionResultPayload = new TransactionResultPayload();
        transactionResultPayload.setTxid(transactionId);
        transactionResult.setPayload(transactionResultPayload);
        transactionResult.setProvider(CryptoCurrencyApiProvider.WHATS_ON_CHAIN);
        return transactionResult;
    }
}
