package ir.kavoshgaran.bitrahcore.core.transactions.coins.zcash;

import ir.kavoshgaran.bitrahcore.core.transactions.CryptoCurrencyPayment;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.TransactionValidator;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class ZcashTransactionValidator implements TransactionValidator {

    @Override
    public boolean validate(BigDecimal fee, BigDecimal value, String to, String from, BigDecimal minValue) {
        if (value.compareTo(fee) < 0)
            throw new CryptoCurrencyApiException(CryptoCurrencyError.TRANSACTION_VALUE_LESS_THAN_FEE);
        if (value.subtract(fee).compareTo(minValue) < 0)
            throw new CryptoCurrencyApiException(CryptoCurrencyError.MIN_VALUE_VIOLATED);
        if (ZcashUtils.isAddressInvalid(to))
            throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
        if (ZcashUtils.isAddressInvalid(from))
            throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
        return true;
    }

    @Override
    public boolean validate(String from, List<CryptoCurrencyPayment> payments, BigDecimal minValue) {
        if (ZcashUtils.isAddressInvalid(from))
            throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
        for (CryptoCurrencyPayment payment : payments) {
            if (payment.getValue().compareTo(minValue) < 0)
                throw new CryptoCurrencyApiException(CryptoCurrencyError.MIN_VALUE_VIOLATED);
            if (ZcashUtils.isAddressInvalid(payment.getTo()))
                throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_ADDRESS);
        }
        return true;
    }

    @Override
    public Coin getCoin() {
        return Coin.ZCASH;
    }
}
