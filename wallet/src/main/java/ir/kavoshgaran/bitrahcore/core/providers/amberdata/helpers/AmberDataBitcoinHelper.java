package ir.kavoshgaran.bitrahcore.core.providers.amberdata.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.AmberDataClient;
import ir.kavoshgaran.bitrahcore.core.transactions.CryptoCurrencyPayment;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericUTXO;
import ir.kavoshgaran.bitrahcore.core.transactions.services.sign.SerializeTransactionService;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class AmberDataBitcoinHelper extends AmberDataGenericHelper {

    private static final Coin COIN = Coin.BITCOIN;

    public AmberDataBitcoinHelper(AmberDataClient amberDataClient) {
        super(amberDataClient);
    }

    @Override
    public String createTransaction(String privateKey, String from, List<CryptoCurrencyPayment> payments, BigDecimal fee) {
        List<GenericUTXO> utxOs = this.getUTXOs(from);
        CryptoCurrencyPayment cryptoCurrencyPayment = payments.get(0);
        return SerializeTransactionService.serializeBitcoinTransaction(privateKey, from, cryptoCurrencyPayment.getTo(), cryptoCurrencyPayment.getValue(), fee, utxOs);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
