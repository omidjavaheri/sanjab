package ir.kavoshgaran.bitrahcore.core.hdwallets;

public class HDWalletCredentials {

    private String mnemonic;
    private String password;

    public String getMnemonic() {
        return mnemonic;
    }

    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
