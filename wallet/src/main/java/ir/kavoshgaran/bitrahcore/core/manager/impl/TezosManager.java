package ir.kavoshgaran.bitrahcore.core.manager.impl;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiService;
import ir.kavoshgaran.bitrahcore.core.manager.CoinManagerAdapter;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class TezosManager extends CoinManagerAdapter {

    @Override
    public Coin getCoin() {
        return Coin.TEZOS;
    }

    @Override
    public List<CryptoCurrencyApiProvider> getPushProviders() {
        return new ArrayList<>(Arrays.asList(
                CryptoCurrencyApiProvider.CRYPTO_NOMIC
        ));
    }

    @Override
    public List<CryptoCurrencyApiProvider> getExplorerProviders() {
        return new ArrayList<>(Arrays.asList(
                CryptoCurrencyApiProvider.CRYPTO_NOMIC
        ));
    }
}
