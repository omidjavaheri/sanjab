package ir.kavoshgaran.bitrahcore.core.transactions.coins.litecoin;

import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericTransaction;

import java.math.BigDecimal;

public class LitecoinTransaction extends GenericTransaction {
    public BigDecimal convertValue(BigDecimal value) {
        return LitecoinUtils.litecoinToSatoshi(value);
    }
}
