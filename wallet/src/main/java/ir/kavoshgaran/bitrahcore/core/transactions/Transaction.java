package ir.kavoshgaran.bitrahcore.core.transactions;

import java.math.BigDecimal;

public interface Transaction {


    SignedTransaction sign(String secret);

    byte[] serializeUnsignedTransaction();
}
