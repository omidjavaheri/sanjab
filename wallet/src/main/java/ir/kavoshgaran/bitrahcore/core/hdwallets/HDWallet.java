package ir.kavoshgaran.bitrahcore.core.hdwallets;

import ir.kavoshgaran.bitrahcore.core.thirdparty.crypto.mnemonic;
import ir.kavoshgaran.bitrahcore.entities.Coin;

import java.math.BigInteger;

public class HDWallet {

    private HDWalletCredentials credentials;
    private BigInteger seed;
    private HDWalletAddress hdWalletAddress;

    public HDWallet(Coin coin, HDWalletCredentials credentials, boolean useSdk) {
        this.credentials = credentials;
        this.seed = mnemonic.seed(credentials.getMnemonic(), credentials.getPassword());
        hdWalletAddress = new HDWalletAddress(coin, seed, useSdk);
    }

    public HDWalletCredentials getCredentials() {
        return credentials;
    }

    public void setCredentials(HDWalletCredentials credentials) {
        this.credentials = credentials;
    }

    public BigInteger getSeed() {
        return seed;
    }

    public void setSeed(BigInteger seed) {
        this.seed = seed;
    }

    public HDWalletAddress getHdWalletAddress() {
        return hdWalletAddress;
    }

    public void setHdWalletAddress(HDWalletAddress hdWalletAddress) {
        this.hdWalletAddress = hdWalletAddress;
    }
}
