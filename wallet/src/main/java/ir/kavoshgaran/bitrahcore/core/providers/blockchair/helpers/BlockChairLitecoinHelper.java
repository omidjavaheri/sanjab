package ir.kavoshgaran.bitrahcore.core.providers.blockchair.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.blockchair.api.BlockChairClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class BlockChairLitecoinHelper extends BlockChairGenericHelper {

    private static final Coin COIN = Coin.LITECOIN;

    public BlockChairLitecoinHelper(BlockChairClient blockChairClient) {
        super(blockChairClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
