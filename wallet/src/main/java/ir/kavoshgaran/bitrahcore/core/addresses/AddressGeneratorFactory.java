package ir.kavoshgaran.bitrahcore.core.addresses;

import ir.kavoshgaran.bitrahcore.core.addresses.cashaddress.BitcoinCashAddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.ethereum.EthereumAddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.legacy.*;
import ir.kavoshgaran.bitrahcore.core.addresses.legacy.BitcoinAddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.legacy.DashAddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.legacy.DogecoinAddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.legacy.LitecoinAddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.tezos.TezosAddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.tron.TronAddressGenerator;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;
import ir.kavoshgaran.bitrahcore.entities.Coin;

public class AddressGeneratorFactory {

    public static AddressGenerator getAddressGenerator(Coin coin) {
        switch (coin) {
            case TETHER:
            case ETHEREUM:
            case ETHEREUM_CLASSIC:
                return new EthereumAddressGenerator();
            case BITCOIN:
            case BITCOINSV:
                return new BitcoinAddressGenerator();
            case DOGECOIN:
                return new DogecoinAddressGenerator();
            case DASH:
                return new DashAddressGenerator();
            case LITECOIN:
                return new LitecoinAddressGenerator();
            case BITCOINCASH:
                return new BitcoinCashAddressGenerator();
            case TRON:
                return new TronAddressGenerator();
            case TEZOS:
                return new TezosAddressGenerator();
            case ZCASH:
                return new ZcashAddressGenerator();
            default:
                throw new CryptoCurrencyApiException(CryptoCurrencyError.COIN_NOT_SUPPORTED);
        }
    }
}
