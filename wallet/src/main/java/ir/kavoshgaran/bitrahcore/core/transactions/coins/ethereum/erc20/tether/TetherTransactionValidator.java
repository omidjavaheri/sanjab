package ir.kavoshgaran.bitrahcore.core.transactions.coins.ethereum.erc20.tether;

import ir.kavoshgaran.bitrahcore.core.transactions.coins.ethereum.EthereumTransactionValidator;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class TetherTransactionValidator extends EthereumTransactionValidator {

    @Override
    public Coin getCoin() {
        return Coin.TETHER;
    }
}
