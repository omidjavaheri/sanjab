package ir.kavoshgaran.bitrahcore.core.providers.whatsonchain.helper;

import ir.kavoshgaran.bitrahcore.core.providers.ApiHelper;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;

public interface WhatsOnChainHelper extends ApiHelper {

    @Override
    default CryptoCurrencyApiProvider getProvider() {
        return CryptoCurrencyApiProvider.WHATS_ON_CHAIN;
    }
}
