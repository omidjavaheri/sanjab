package ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoin;

import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericTransaction;

import java.math.BigDecimal;

public class BitcoinTransaction extends GenericTransaction {
    public BigDecimal convertValue(BigDecimal value) {
        return BitcoinUtils.bitcoinToSatoshi(value);
    }
}
