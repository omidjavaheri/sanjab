package ir.kavoshgaran.bitrahcore.core.transactions.services.sign;

public class SerializeTransactionException extends RuntimeException {

    public SerializeTransactionException(String message) {
        super(message);
    }
}
