package ir.kavoshgaran.bitrahcore.core.providers.tokenview.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.tokenview.api.TokenViewClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class TokenViewLitecoinHelper extends TokenViewGenericHelper {

    private static final Coin COIN = Coin.LITECOIN;

    public TokenViewLitecoinHelper(TokenViewClient tokenViewClient) {
        super(tokenViewClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
