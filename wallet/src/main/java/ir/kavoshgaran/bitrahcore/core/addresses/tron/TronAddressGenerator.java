package ir.kavoshgaran.bitrahcore.core.addresses.tron;

import ir.kavoshgaran.bitrahcore.core.addresses.AddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.GeneratedAddress;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoSdkService;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TronAddressGenerator implements AddressGenerator {

    private final Logger logger = LoggerFactory.getLogger(TronAddressGenerator.class);

    public static void main(String[] args) {
        new TronAddressGenerator().generate();
    }

    @Override
    public GeneratedAddress generate() {
        GeneratedAddress generatedAddress = CryptoSdkService.generateAddress(Coin.TRON);
        logger.debug(String.format("Tron Address Generator -> PrivateKey: %s || Address: %S", generatedAddress.getPrivateKey(), generatedAddress.getAddress()));
        return generatedAddress;
    }
}
