package ir.kavoshgaran.bitrahcore.core.providers.sochain.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.sochain.api.SoChainClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class SoChainLitecoinHelper extends SoChainGenericHelper {

    public SoChainLitecoinHelper(SoChainClient soChainClient) {
        super(soChainClient);
    }

    @Override
    public Coin getCoin() {
        return Coin.LITECOIN;
    }

}
