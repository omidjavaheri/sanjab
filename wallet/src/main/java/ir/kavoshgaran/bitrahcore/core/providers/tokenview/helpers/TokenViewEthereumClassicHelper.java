package ir.kavoshgaran.bitrahcore.core.providers.tokenview.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.tokenview.api.TokenViewClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class TokenViewEthereumClassicHelper extends TokenViewEthereumHelper {

    private static final Coin COIN = Coin.ETHEREUM_CLASSIC;

    public TokenViewEthereumClassicHelper(TokenViewClient tokenViewClient) {
        super(tokenViewClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
