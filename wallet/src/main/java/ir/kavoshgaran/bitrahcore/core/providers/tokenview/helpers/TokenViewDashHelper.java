package ir.kavoshgaran.bitrahcore.core.providers.tokenview.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.tokenview.api.TokenViewClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class TokenViewDashHelper extends TokenViewGenericHelper {

    private static final Coin COIN = Coin.DASH;

    public TokenViewDashHelper(TokenViewClient tokenViewClient) {
        super(tokenViewClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
