package ir.kavoshgaran.bitrahcore.core.transactions;

import ir.kavoshgaran.bitrahcore.utils.BitrahHashUtils;
import ir.kavoshgaran.bitrahcore.utils.BitrahHexUtils;

public class SignedTransaction {
    byte[] serializedTx;

    public SignedTransaction(byte[] serializedTx) {
        this.serializedTx = serializedTx;
    }

    public String toHexString() {
        return BitrahHexUtils.bytesToHex(serializedTx);
    }

    public String hashSha256() {
        return BitrahHashUtils.sha256(serializedTx);
    }

    public String doubleSha256() {
        return BitrahHashUtils.doubleSha256(serializedTx);
    }
}
