package ir.kavoshgaran.bitrahcore.core.providers.amberdata.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.models.AmberDataTransaction;
import ir.kavoshgaran.bitrahcore.core.transactions.CryptoCurrencyPayment;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoincash.BitcoinCashTransactionBuilder;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoincash.BitcoinCashUtils;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericTransaction;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericUTXO;
import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.AmberDataClient;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.PaymentDetails;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class AmberDataBitcoinCashHelper extends AmberDataGenericHelper {

    private static final Coin COIN = Coin.BITCOINCASH;

    public AmberDataBitcoinCashHelper(AmberDataClient amberDataClient) {
        super(amberDataClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }

    @Override
    public String generatePublicKeyScript(String address) {
        return BitcoinCashUtils.generatePublicKeyScript(address);
    }

    @Override
    public String createTransaction(String privateKey, String from, List<CryptoCurrencyPayment> payments, BigDecimal fee) {
        List<GenericUTXO> genericUTXOS = getUTXOs(from);
        GenericTransaction genericTransaction = new BitcoinCashTransactionBuilder().build(getCoin(), genericUTXOS, from, payments, fee);
        return genericTransaction.serialize(privateKey);
    }

    @Override
    public String processAddress(String address) {
        return address.replace("bitcoincash:", "");
    }
}
