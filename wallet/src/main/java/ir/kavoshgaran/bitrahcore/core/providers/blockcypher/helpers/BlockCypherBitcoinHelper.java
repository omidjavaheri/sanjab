package ir.kavoshgaran.bitrahcore.core.providers.blockcypher.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.blockcypher.api.BlockCypherClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class BlockCypherBitcoinHelper extends BlockCypherGenericHelper {

    private static final Coin COIN = Coin.BITCOIN;

    public BlockCypherBitcoinHelper(BlockCypherClient blockCypherClient) {
        super(blockCypherClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
