package ir.kavoshgaran.bitrahcore.core.providers.tronprovider;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiServiceAdapter;
import ir.kavoshgaran.bitrahcore.core.providers.tronprovider.api.TronGridApisClient;
import ir.kavoshgaran.bitrahcore.core.providers.tronprovider.api.models.response.TronGridPushTransactionResponse;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResultPayload;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(5)
public class TronProviderApisService extends CryptoCurrencyApiServiceAdapter {

    private final TronGridApisClient tronGridApisClient;

    public TronProviderApisService(TronGridApisClient tronGridApisClient) {
        this.tronGridApisClient = tronGridApisClient;
    }

    @Override
    public CryptoCurrencyApiProvider getApiProvider() {
        return CryptoCurrencyApiProvider.TRON_PROVIDER;
    }

    @Override
    public TransactionResult pushTransaction(Coin coin, String transactionHex) {
        TronGridPushTransactionResponse response = tronGridApisClient.pushTransaction(transactionHex);
        if (response.isResult()) {
            TransactionResult transactionResult = new TransactionResult();
            TransactionResultPayload transactionResultPayload = new TransactionResultPayload();
            transactionResultPayload.setTxid(response.getTxid());
            transactionResult.setPayload(transactionResultPayload);
            transactionResult.setProvider(CryptoCurrencyApiProvider.TRON_PROVIDER);
            return transactionResult;
        } else {
            throw new CryptoCurrencyApiException(CryptoCurrencyError.INVALID_TRANSACTION);
        }
    }
}
