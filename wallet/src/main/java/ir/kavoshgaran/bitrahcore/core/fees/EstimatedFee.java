package ir.kavoshgaran.bitrahcore.core.fees;

import ir.kavoshgaran.bitrahcore.entities.Coin;

import java.math.BigDecimal;

public interface EstimatedFee {
    Coin getCoin();

    BigDecimal getInstantFee();

    BigDecimal getEvacuationFee();
}
