package ir.kavoshgaran.bitrahcore.core.providers.sochain.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.ApiHelper;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;

public interface SoChainHelper extends ApiHelper {

    @Override
    default CryptoCurrencyApiProvider getProvider() {
        return CryptoCurrencyApiProvider.SO_CHAIN;
    }
}
