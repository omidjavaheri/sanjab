package ir.kavoshgaran.bitrahcore.core.transactions.coins.ethereum;

import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class EthereumClassicTransactionValidator extends EthereumTransactionValidator {

    @Override
    public Coin getCoin() {
        return Coin.ETHEREUM_CLASSIC;
    }
}
