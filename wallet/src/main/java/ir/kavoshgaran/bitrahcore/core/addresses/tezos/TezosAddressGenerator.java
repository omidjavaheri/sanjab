package ir.kavoshgaran.bitrahcore.core.addresses.tezos;


import com.goterl.lazycode.lazysodium.utils.KeyPair;
import ir.kavoshgaran.bitrahcore.core.addresses.AddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.GeneratedAddress;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.tezos.TezosUtils;
import ir.kavoshgaran.bitrahcore.utils.Base58Utils;
import ir.kavoshgaran.bitrahcore.utils.BitrahByteUtils;
import ir.kavoshgaran.bitrahcore.utils.BitrahCheckSumUtils;
import ir.kavoshgaran.bitrahcore.utils.BitrahSodiumUtils;

public class TezosAddressGenerator implements AddressGenerator {

    public static void main(String[] args) {
        new TezosAddressGenerator().generate();
    }

    @Override
    public GeneratedAddress generate() {
        KeyPair keyPair = BitrahSodiumUtils.generateCryptoSignKeyPair();

        byte[] publicKeyHashBytes = BitrahSodiumUtils.cryptoGenericHash(20, keyPair.getPublicKey().getAsBytes());
        byte[] concatenate = BitrahByteUtils.concatenate(TezosUtils.Prefix.TZ1, publicKeyHashBytes);
        byte[] publicKeyHashCheckSum = BitrahCheckSumUtils.doubleSha256(concatenate);
        String publicKeyHash = Base58Utils.encode(concatenate, publicKeyHashCheckSum);

        return new GeneratedAddress(keyPair.getSecretKey().getAsHexString(), publicKeyHash);
    }

    public static boolean validateAddress(String address) {
        return address.startsWith("tz") && address.length() != 36;
    }
}
