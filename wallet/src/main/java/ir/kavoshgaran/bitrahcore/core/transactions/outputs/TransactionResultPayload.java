package ir.kavoshgaran.bitrahcore.core.transactions.outputs;

public class TransactionResultPayload {

    private String txid;

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }
}
