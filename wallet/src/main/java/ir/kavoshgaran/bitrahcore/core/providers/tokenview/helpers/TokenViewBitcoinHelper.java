package ir.kavoshgaran.bitrahcore.core.providers.tokenview.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.tokenview.api.TokenViewClient;
import ir.kavoshgaran.bitrahcore.core.transactions.CryptoCurrencyPayment;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericUTXO;
import ir.kavoshgaran.bitrahcore.core.transactions.services.sign.SerializeTransactionService;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class TokenViewBitcoinHelper extends TokenViewGenericHelper {

    public TokenViewBitcoinHelper(TokenViewClient tokenViewClient) {
        super(tokenViewClient);
    }

    @Override
    public Coin getCoin() {
        return Coin.BITCOIN;
    }

    @Override
    public String createTransaction(String privateKey, String from, List<CryptoCurrencyPayment> payments, BigDecimal fee) {
        List<GenericUTXO> generatedUTXOs = super.getGeneratedUTXOs(from);
        CryptoCurrencyPayment cryptoCurrencyPayment = payments.get(0);
        return SerializeTransactionService.serializeBitcoinTransaction(privateKey, from, cryptoCurrencyPayment.getTo(), cryptoCurrencyPayment.getValue(), fee, generatedUTXOs);
    }
}
