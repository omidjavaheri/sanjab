package ir.kavoshgaran.bitrahcore.core.price;

import ir.kavoshgaran.bitrahcore.entities.CurrencyPrice;
import org.jsoup.nodes.Element;

public interface CurrencyPriceParser {

    CurrencyPrice parseElement(Element element);
}
