package ir.kavoshgaran.bitrahcore.core.addresses.ripple;

import ir.kavoshgaran.bitrahcore.core.addresses.GeneratedAddress;
import ir.kavoshgaran.bitrahcore.core.addresses.legacy.LegacyAddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.legacy.LegacyAddressVariant;
import ir.kavoshgaran.bitrahcore.core.addresses.legacy.WifGenerator;
import ir.kavoshgaran.bitrahcore.utils.BitrahByteUtils;
import ir.kavoshgaran.bitrahcore.utils.BitrahHashUtils;
import ir.kavoshgaran.bitrahcore.utils.BitrahHexUtils;
import ir.kavoshgaran.bitrahcore.utils.RippleBase58Utils;
import ir.kavoshgaran.bitrahcore.utils.signature.ECKeyPair;
import ir.kavoshgaran.bitrahcore.utils.signature.ECSignUtils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

public class RippleAddressGenerator {

    private final static int SEED_LENGTH = 16;

    private final LegacyAddressVariant variant = LegacyAddressVariant.RIPPLE;

    public static void main(String[] args) throws NoSuchAlgorithmException {
        new RippleAddressGenerator().generate();
    }

    public GeneratedAddress generate() throws NoSuchAlgorithmException {
        byte[] seed = new byte[SEED_LENGTH];
        SecureRandom.getInstanceStrong().nextBytes(seed);

        ECKeyPair keyPair = ECSignUtils.generateECKeyPair(seed);
        byte[] compressedPubKey = BitrahHexUtils.decodeHexString(
                ECSignUtils.computePublicKey(keyPair.getPrivateKey(), true));
        String secret = encodeToBytesChecked(seed, new byte[]{variant.getWifPrefix()});

        byte[] a1 = LegacyAddressGenerator.publicKeyToAddress(compressedPubKey, variant);
        String address = RippleBase58Utils.encode(a1);


        System.out.println(address);
        System.out.println(secret);
        System.out.println(WifGenerator.generateWif(keyPair.getPrivateKey(), LegacyAddressVariant.RIPPLE, true));

        return new GeneratedAddress("", "address");
    }

    public String encodeToBytesChecked(byte[] input, byte[] version) {
        byte[] buffer = new byte[input.length + version.length];
        System.arraycopy(version, 0, buffer, 0, version.length);
        System.arraycopy(input, 0, buffer, version.length, input.length);
        byte[] checkSum = BitrahByteUtils.copyOfRange(BitrahHashUtils.doubleSha256(buffer), 0, 4);
        byte[] output = new byte[buffer.length + checkSum.length];
        System.arraycopy(buffer, 0, output, 0, buffer.length);
        System.arraycopy(checkSum, 0, output, buffer.length, checkSum.length);
        return RippleBase58Utils.encode(output);
    }

    public boolean validateAddress(String address) {
        byte[] decodedBytes;

        try {
            decodedBytes = RippleBase58Utils.decode(address);
        } catch (Exception e) {
            return false;
        }

        byte[] hash160Bytes = Arrays.copyOfRange(decodedBytes, 0, decodedBytes.length - 4);
        if (decodedBytes[0] != variant.getVersion()) return false;


        int versionByteLength = 1;

        if (hash160Bytes.length != 20 + versionByteLength) return false;

        byte[] finalEncodedHash = BitrahHashUtils.doubleSha256Bytes(hash160Bytes);
        for (int i = 0; i < 4; i++) {
            if (decodedBytes[20 + versionByteLength + i] != finalEncodedHash[i]) return false;
        }

        return true;
    }
}
