package ir.kavoshgaran.bitrahcore.core.providers.zcha;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiServiceAdapter;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Service;

@Service
public class ZchaService extends CryptoCurrencyApiServiceAdapter {

    @Override
    public CryptoCurrencyApiProvider getApiProvider() {
        return CryptoCurrencyApiProvider.ZCHA;
    }

    @Override
    public TransactionResult pushTransaction(Coin coin, String transactionHex) {
        throw new CryptoCurrencyApiException(CryptoCurrencyError.API_ERROR);
    }
}
