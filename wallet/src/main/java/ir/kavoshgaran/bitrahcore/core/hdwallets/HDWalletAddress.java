package ir.kavoshgaran.bitrahcore.core.hdwallets;

import ir.kavoshgaran.bitrahcore.core.thirdparty.crypto.hdwallet;
import ir.kavoshgaran.bitrahcore.core.thirdparty.crypto.wallet;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;
import ir.kavoshgaran.bitrahcore.entities.Coin;

import java.math.BigInteger;

public class HDWalletAddress {

    private Coin coin;
    private String xPrivateKey;
    private String xPublicKey;
    private String privateKey;
    private String publicKey;
    private String address;

    public HDWalletAddress(Coin coin, BigInteger seed, boolean useSdk) {
        this.coin = coin;
        if (useSdk) {
            if (coin.getName() == null)
                throw new CryptoCurrencyApiException(CryptoCurrencyError.COIN_NOT_SUPPORTED);
            String coinName = coin.getName();
            xPrivateKey = hdwallet.xprivatekey_master(seed, coinName, false);
            xPublicKey = hdwallet.xpublickey_from_xprivatekey(xPrivateKey, coinName, false);
            privateKey = hdwallet.privatekey_from_xprivatekey(xPrivateKey, true, coinName, false);
            publicKey = hdwallet.publickey_from_xpublickey(xPublicKey, true, coinName, false);
            address = wallet.address_from_publickey(publicKey, coinName, false);
        }
    }

    public Coin getCoin() {
        return coin;
    }

    public void setCoin(Coin coin) {
        this.coin = coin;
    }

    public String getXPrivateKey() {
        return xPrivateKey;
    }

    public void setXPrivateKey(String xPrivateKey) {
        this.xPrivateKey = xPrivateKey;
    }

    public String getXPublicKey() {
        return xPublicKey;
    }

    public void setXPublicKey(String xPublicKey) {
        this.xPublicKey = xPublicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
