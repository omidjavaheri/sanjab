package ir.kavoshgaran.bitrahcore.core.manager.impl;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiService;
import ir.kavoshgaran.bitrahcore.core.manager.CoinManagerAdapter;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class TronManager extends CoinManagerAdapter {

    @Override
    public Coin getCoin() {
        return Coin.TRON;
    }

    @Override
    public List<CryptoCurrencyApiProvider> getPushProviders() {
        return new ArrayList<>(Arrays.asList(
                CryptoCurrencyApiProvider.TRON_PROVIDER
        ));
    }

    @Override
    public List<CryptoCurrencyApiProvider> getExplorerProviders() {
        return new ArrayList<>(Arrays.asList(
                CryptoCurrencyApiProvider.TRON_PROVIDER
        ));
    }
}
