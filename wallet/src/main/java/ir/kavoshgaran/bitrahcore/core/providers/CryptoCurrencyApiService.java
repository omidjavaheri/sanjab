package ir.kavoshgaran.bitrahcore.core.providers;

import ir.kavoshgaran.bitrahcore.core.transactions.CryptoCurrencyPayment;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.entities.Coin;

import java.math.BigDecimal;
import java.util.List;

public interface CryptoCurrencyApiService {

    ApiHelper getHelper(Coin coin);

    CryptoCurrencyApiProvider getApiProvider();

    TransactionResult pushTransaction(Coin coin, String privateKey, String from, String to, BigDecimal value, BigDecimal fee);

    TransactionResult pushTransaction(Coin coin, String privateKey, String from, List<CryptoCurrencyPayment> payments, BigDecimal fee);

    TransactionResult pushTransaction(Coin coin, String transactionHex);

    Long getLockTimePeriod();

    boolean isLocked();

    boolean isAccessible();

    void lock();
}
