package ir.kavoshgaran.bitrahcore.core.providers.btcexplorer.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.btcexplorer.api.BtcExplorerClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class BtcExplorerBitcoinHelper extends BtcExplorerGenericHelper {

    private static final Coin COIN = Coin.BITCOIN;

    public BtcExplorerBitcoinHelper(BtcExplorerClient btcExplorerClient) {
        super(btcExplorerClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
