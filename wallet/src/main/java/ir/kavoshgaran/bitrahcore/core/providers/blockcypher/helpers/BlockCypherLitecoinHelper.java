package ir.kavoshgaran.bitrahcore.core.providers.blockcypher.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.blockcypher.api.BlockCypherClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class BlockCypherLitecoinHelper extends BlockCypherGenericHelper {

    private static final Coin COIN = Coin.LITECOIN;

    public BlockCypherLitecoinHelper(BlockCypherClient blockCypherClient) {
        super(blockCypherClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
