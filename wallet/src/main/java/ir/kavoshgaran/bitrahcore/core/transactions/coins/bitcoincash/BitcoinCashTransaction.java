package ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoincash;

import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericForkTransaction;

import java.math.BigDecimal;

public class BitcoinCashTransaction extends GenericForkTransaction {
    public BigDecimal convertValue(BigDecimal value) {
        return BitcoinCashUtils.bitcoinCashToSatoshi(value);
    }
}
