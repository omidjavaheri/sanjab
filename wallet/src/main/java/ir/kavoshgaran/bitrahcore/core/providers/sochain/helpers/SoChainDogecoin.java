package ir.kavoshgaran.bitrahcore.core.providers.sochain.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.sochain.api.SoChainClient;
import ir.kavoshgaran.bitrahcore.core.transactions.CryptoCurrencyPayment;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericUTXO;
import ir.kavoshgaran.bitrahcore.core.transactions.services.sign.SerializeTransactionService;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class SoChainDogecoin extends SoChainGenericHelper {

    public SoChainDogecoin(SoChainClient soChainClient) {
        super(soChainClient);
    }

    @Override
    public Coin getCoin() {
        return Coin.DOGECOIN;
    }

}
