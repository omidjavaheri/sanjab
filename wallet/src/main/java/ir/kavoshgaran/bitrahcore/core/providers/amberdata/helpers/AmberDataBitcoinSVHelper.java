package ir.kavoshgaran.bitrahcore.core.providers.amberdata.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.AmberDataClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class AmberDataBitcoinSVHelper extends AmberDataGenericHelper {

    private static final Coin COIN = Coin.BITCOINSV;

    public AmberDataBitcoinSVHelper(AmberDataClient amberDataClient) {
        super(amberDataClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
