package ir.kavoshgaran.bitrahcore.core.providers.cryptoapis.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.cryptoapis.api.CryptoApisClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class CryptoApisBitcoinHelper extends CryptoApisGenericHelper {

    private static final Coin COIN = Coin.BITCOIN;

    public CryptoApisBitcoinHelper(CryptoApisClient cryptoApisClient) {
        super(cryptoApisClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }

}
