package ir.kavoshgaran.bitrahcore.core.addresses.legacy;

import ir.kavoshgaran.bitrahcore.utils.Base58Utils;
import ir.kavoshgaran.bitrahcore.utils.BitrahHashUtils;
import ir.kavoshgaran.bitrahcore.utils.BitrahHexUtils;
import ir.kavoshgaran.bitrahcore.utils.RippleBase58Utils;

import java.util.Arrays;

public class WifGenerator {

    public static String generateWif(byte[] privateKey, LegacyAddressVariant variant, boolean userRippleBase58) {

        byte[] toBeHashed = new byte[privateKey.length + 1];
        toBeHashed[0] = variant.getWifPrefix();
        System.arraycopy(privateKey, 0, toBeHashed, 1, privateKey.length);
        String wifHashed = BitrahHashUtils.doubleSha256(BitrahHexUtils.bytesToHex(toBeHashed));

        byte[] checksum = BitrahHexUtils.decodeHexString(wifHashed);
        byte[] wif = new byte[privateKey.length + 5];
        wif[0] = variant.getWifPrefix();
        System.arraycopy(privateKey, 0, wif, 1, privateKey.length);
        System.arraycopy(checksum, 0, wif, privateKey.length + 1, 4);
        return userRippleBase58 ? RippleBase58Utils.encode(wif) : Base58Utils.encode(wif);
    }

    public static String generateWif(byte[] privateKey, LegacyAddressVariant variant) {
        return generateWif(privateKey, variant, false);
    }

    public static String convertWifToPrivateKey(String wif, boolean userRippleBase58) {
        byte[] decoded = userRippleBase58 ? RippleBase58Utils.decode(wif) : Base58Utils.decode(wif);
        byte[] keyBytes = Arrays.copyOfRange(decoded, 1, decoded.length - 4);
        return BitrahHexUtils.bytesToHex(keyBytes);
    }

    public static String convertWifToPrivateKey(String wif) {
        return convertWifToPrivateKey(wif, false);
    }
}
