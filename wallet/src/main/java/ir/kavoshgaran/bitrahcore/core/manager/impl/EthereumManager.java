package ir.kavoshgaran.bitrahcore.core.manager.impl;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiService;
import ir.kavoshgaran.bitrahcore.core.manager.CoinManagerAdapter;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class EthereumManager extends CoinManagerAdapter {

    @Override
    public Coin getCoin() {
        return Coin.ETHEREUM;
    }

    @Override
    public List<CryptoCurrencyApiProvider> getPushProviders() {
        return new ArrayList<>(Arrays.asList(
                CryptoCurrencyApiProvider.BLOCK_CHAIR,
                CryptoCurrencyApiProvider.AMBER_DATA,
                CryptoCurrencyApiProvider.BLOCK_CYPHER,
                CryptoCurrencyApiProvider.CRYPTO_APIS
        ));
    }

    @Override
    public List<CryptoCurrencyApiProvider> getExplorerProviders() {
        return new ArrayList<>(Arrays.asList(
                CryptoCurrencyApiProvider.AMBER_DATA,
                CryptoCurrencyApiProvider.BLOCK_CHAIR,
                CryptoCurrencyApiProvider.BLOCK_CYPHER,
                CryptoCurrencyApiProvider.CRYPTO_APIS,
                CryptoCurrencyApiProvider.TOKEN_VIEW
                ));
    }
}
