package ir.kavoshgaran.bitrahcore.core.transactions.excoino;

import java.math.BigDecimal;

public class ExcoinoResult {

    private BigDecimal sourceToFiatRatio;
    private BigDecimal destinationToFiatRatio;
    private BigDecimal conversionResult;

    public ExcoinoResult(BigDecimal sourceToFiatRatio, BigDecimal destinationToFiatRatio, BigDecimal conversionResult) {
        this.sourceToFiatRatio = sourceToFiatRatio;
        this.destinationToFiatRatio = destinationToFiatRatio;
        this.conversionResult = conversionResult;
    }

    public BigDecimal getSourceToFiatRatio() {
        return sourceToFiatRatio;
    }

    public void setSourceToFiatRatio(BigDecimal sourceToFiatRatio) {
        this.sourceToFiatRatio = sourceToFiatRatio;
    }

    public BigDecimal getDestinationToFiatRatio() {
        return destinationToFiatRatio;
    }

    public void setDestinationToFiatRatio(BigDecimal destinationToFiatRatio) {
        this.destinationToFiatRatio = destinationToFiatRatio;
    }

    public BigDecimal getConversionResult() {
        return conversionResult;
    }

    public void setConversionResult(BigDecimal conversionResult) {
        this.conversionResult = conversionResult;
    }
}
