package ir.kavoshgaran.bitrahcore.core.providers.bitcoincom;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiServiceAdapter;
import ir.kavoshgaran.bitrahcore.core.providers.bitcoincom.api.BitcoinComClient;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResultPayload;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Service;

@Service
public class BitcoinComService extends CryptoCurrencyApiServiceAdapter {

    private final BitcoinComClient bitcoinComClient;

    public BitcoinComService(BitcoinComClient bitcoinComClient) {
        this.bitcoinComClient = bitcoinComClient;
    }

    @Override
    public CryptoCurrencyApiProvider getApiProvider() {
        return CryptoCurrencyApiProvider.BITCOIN_COM;
    }

    @Override
    public TransactionResult pushTransaction(Coin coin, String transactionHex) {
        String transactionId = this.bitcoinComClient.pushTransaction(transactionHex);
        TransactionResult transactionResult = new TransactionResult();
        TransactionResultPayload transactionResultPayload = new TransactionResultPayload();
        transactionResultPayload.setTxid(transactionId);
        transactionResult.setPayload(transactionResultPayload);
        transactionResult.setProvider(CryptoCurrencyApiProvider.BITCOIN_COM);
        return transactionResult;
    }
}
