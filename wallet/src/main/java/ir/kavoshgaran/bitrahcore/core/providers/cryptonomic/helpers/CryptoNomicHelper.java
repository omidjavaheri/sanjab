package ir.kavoshgaran.bitrahcore.core.providers.cryptonomic.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.ApiHelper;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;

public interface CryptoNomicHelper extends ApiHelper {

    @Override
    default CryptoCurrencyApiProvider getProvider() {
        return CryptoCurrencyApiProvider.CRYPTO_NOMIC;
    }
}
