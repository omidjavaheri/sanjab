package ir.kavoshgaran.bitrahcore.core.providers.cryptoapis.helpers;

import ir.kavoshgaran.bitrahcore.core.providers.cryptoapis.api.CryptoApisClient;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

@Component
public class CryptoApisBitcoinSVHelper extends CryptoApisGenericHelper {

    private static final Coin COIN = Coin.BITCOINSV;

    public CryptoApisBitcoinSVHelper(CryptoApisClient cryptoApisClient) {
        super(cryptoApisClient);
    }

    @Override
    public Coin getCoin() {
        return COIN;
    }
}
