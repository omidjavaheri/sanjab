package ir.kavoshgaran.bitrahcore.core.transactions.coins;

import ir.kavoshgaran.bitrahcore.entities.Coin;

public interface AbstractCoin {

    Coin getCoin();
}
