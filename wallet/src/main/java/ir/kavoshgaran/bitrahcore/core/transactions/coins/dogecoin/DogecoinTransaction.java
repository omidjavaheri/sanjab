package ir.kavoshgaran.bitrahcore.core.transactions.coins.dogecoin;

import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericTransaction;
import ir.kavoshgaran.bitrahcore.core.transactions.coins.litecoin.LitecoinUtils;

import java.math.BigDecimal;

public class DogecoinTransaction extends GenericTransaction {
    public BigDecimal convertValue(BigDecimal value) {
        return LitecoinUtils.litecoinToSatoshi(value);
    }
}
