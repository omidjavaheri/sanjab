package ir.kavoshgaran.bitrahcore.core.providers.tokenview;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiServiceAdapter;
import ir.kavoshgaran.bitrahcore.core.providers.tokenview.api.TokenViewClient;
import ir.kavoshgaran.bitrahcore.core.providers.tokenview.api.models.response.TokenViewPushTransactionResponse;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResultPayload;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Service;

@Service
public class TokenViewService extends CryptoCurrencyApiServiceAdapter {

    private final TokenViewClient tokenViewClient;

    public TokenViewService(TokenViewClient tokenViewClient) {
        this.tokenViewClient = tokenViewClient;
    }

    @Override
    public CryptoCurrencyApiProvider getApiProvider() {
        return CryptoCurrencyApiProvider.TOKEN_VIEW;
    }

    @Override
    public TransactionResult pushTransaction(Coin coin, String transactionHex) {
        TokenViewPushTransactionResponse pushTransactionResponse = this.tokenViewClient.pushTransaction(coin, transactionHex);
        TransactionResult transactionResult = new TransactionResult();
        TransactionResultPayload transactionResultPayload = new TransactionResultPayload();
        transactionResultPayload.setTxid(pushTransactionResponse.getResult());
        transactionResult.setPayload(transactionResultPayload);
        transactionResult.setProvider(CryptoCurrencyApiProvider.TOKEN_VIEW);
        return transactionResult;
    }
}
