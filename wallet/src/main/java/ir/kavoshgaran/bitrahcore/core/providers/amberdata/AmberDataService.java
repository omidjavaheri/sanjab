package ir.kavoshgaran.bitrahcore.core.providers.amberdata;

import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiServiceAdapter;
import ir.kavoshgaran.bitrahcore.core.providers.amberdata.api.AmberDataClient;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResult;
import ir.kavoshgaran.bitrahcore.core.transactions.outputs.TransactionResultPayload;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(5)
public class AmberDataService extends CryptoCurrencyApiServiceAdapter {


    private final AmberDataClient amberDataClient;

    public AmberDataService(AmberDataClient amberDataClient) {
        this.amberDataClient = amberDataClient;
    }

    @Override
    public TransactionResult pushTransaction(Coin coin, String transactionHex) {
        if (coin.isSecondLayer())
            coin = coin.getBaseCoin();

        String txHash = amberDataClient.pushTransaction(coin, transactionHex);

        TransactionResult txResult = new TransactionResult();
        TransactionResultPayload payload = new TransactionResultPayload();
        payload.setTxid(txHash);
        txResult.setPayload(payload);
        txResult.setProvider(CryptoCurrencyApiProvider.AMBER_DATA);

        return txResult;
    }

    @Override
    public CryptoCurrencyApiProvider getApiProvider() {
        return CryptoCurrencyApiProvider.AMBER_DATA;
    }

}
