package ir.kavoshgaran.bitrahcore.core.manager.impl;

import ir.kavoshgaran.bitrahcore.core.manager.CoinManagerAdapter;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiService;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Component
public class BitcoinManager extends CoinManagerAdapter {

    @Override
    public Coin getCoin() {
        return Coin.BITCOIN;
    }

    @Override
    public List<CryptoCurrencyApiProvider> getPushProviders() {
        return new ArrayList<>(Arrays.asList(
                CryptoCurrencyApiProvider.BLOCK_CHAIR,
                CryptoCurrencyApiProvider.SO_CHAIN,
                CryptoCurrencyApiProvider.AMBER_DATA,
                CryptoCurrencyApiProvider.BLOCK_CHAIN,
                CryptoCurrencyApiProvider.BLOCK_CYPHER,
                CryptoCurrencyApiProvider.CRYPTO_APIS
        ));
    }

    @Override
    public List<CryptoCurrencyApiProvider> getExplorerProviders() {
        return new ArrayList<>(Arrays.asList(
                CryptoCurrencyApiProvider.SO_CHAIN,
                CryptoCurrencyApiProvider.AMBER_DATA,
                CryptoCurrencyApiProvider.BLOCK_CHAIR,
                CryptoCurrencyApiProvider.BTC_EXPLORER,
                CryptoCurrencyApiProvider.TOKEN_VIEW,
                CryptoCurrencyApiProvider.BLOCK_CHAIN,
                CryptoCurrencyApiProvider.BLOCK_CYPHER,
                CryptoCurrencyApiProvider.CRYPTO_APIS
        ));
    }
}
