package ir.kavoshgaran.bitrahcore.core.transactions.coins.bitcoincash;

import ir.kavoshgaran.bitrahcore.core.transactions.coins.generic.GenericTransactionBuilder;

public class BitcoinCashTransactionBuilder extends GenericTransactionBuilder {

    @Override
    public String generatePublicKeyScript(String address) {
        return BitcoinCashUtils.generatePublicKeyScript(address);
    }
}
