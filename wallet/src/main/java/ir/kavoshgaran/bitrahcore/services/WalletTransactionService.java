package ir.kavoshgaran.bitrahcore.services;

import ir.kavoshgaran.bitrahcore.core.transactions.outputs.PaymentDetails;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import ir.kavoshgaran.bitrahcore.services.dto.WalletTransactionDTO;
import ir.kavoshgaran.bitrahcore.services.dto.WalletTransactionInquiryDTO;

public interface WalletTransactionService {

    String signedTransaction(Coin coin, WalletTransactionDTO walletTransactionDTO);

    String pushRawTransaction(Coin coin, WalletTransactionDTO walletTransactionDTO);

    String pushHexTransaction(Coin coin, String transactionHex);

    WalletTransactionInquiryDTO inquiryTransaction(Coin coin, String transactionId);

    PaymentDetails inquiryTransactionByIdAndAddress(Coin coin, String transactionId, String address);
}
