package ir.kavoshgaran.bitrahcore.services.impl;

import ir.kavoshgaran.bitrahcore.core.addresses.AddressGenerator;
import ir.kavoshgaran.bitrahcore.core.addresses.AddressGeneratorFactory;
import ir.kavoshgaran.bitrahcore.core.addresses.GeneratedAddress;
import ir.kavoshgaran.bitrahcore.core.hdwallets.HDWallet;
import ir.kavoshgaran.bitrahcore.core.hdwallets.HDWalletFactory;
import ir.kavoshgaran.bitrahcore.core.hdwallets.HDWalletGenerator;
import ir.kavoshgaran.bitrahcore.core.hdwallets.MnemonicHelper;
import ir.kavoshgaran.bitrahcore.core.manager.CoinManager;
import ir.kavoshgaran.bitrahcore.core.manager.CoinManagerFactory;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;
import ir.kavoshgaran.bitrahcore.core.transactions.services.AccountStatus;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import ir.kavoshgaran.bitrahcore.entities.Wallet;
import ir.kavoshgaran.bitrahcore.repositories.WalletRepository;
import ir.kavoshgaran.bitrahcore.services.WalletService;
import ir.kavoshgaran.bitrahcore.services.dto.WalletBalanceDTO;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Map;

@Service
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final CoinManagerFactory coinManagerFactory;

    public WalletServiceImpl(WalletRepository walletRepository,
                             CoinManagerFactory coinManagerFactory) {
        this.walletRepository = walletRepository;
        this.coinManagerFactory = coinManagerFactory;
    }

    @Override
    public String generateWallet(Coin coin) {
        AddressGenerator addressGenerator = AddressGeneratorFactory.getAddressGenerator(coin);
        try {
            GeneratedAddress generatedAddress = addressGenerator.generate();
            Wallet wallet = new Wallet();
            wallet.setCoin(coin);
            wallet.setAddress(generatedAddress.getAddress());
            wallet.setPrivateKey(generatedAddress.getPrivateKey());
            wallet.setCreatedDate(ZonedDateTime.now());
            this.walletRepository.save(wallet);
            return generatedAddress.getAddress();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new CryptoCurrencyApiException(CryptoCurrencyError.ADDRESS_GENERATE_ERROR);
        }
    }

    @Override
    public HDWallet generateHDWallet(Coin coin, String password) {
        HDWalletGenerator hdWalletGenerator = HDWalletFactory.getHDWalletGenerator(coin);
        HDWallet generatedHdWallet = hdWalletGenerator.generate(coin, password);
        persistHDWallet(coin, generatedHdWallet);
        return generatedHdWallet;
    }

    @Override
    public HDWallet retrieveHDWallet(Coin coin, String mnemonic, String password) {
        HDWalletGenerator hdWalletGenerator = HDWalletFactory.getHDWalletGenerator(coin);
        HDWallet generatedHdWallet = hdWalletGenerator.retrieve(coin, mnemonic, password);
        persistHDWallet(coin, generatedHdWallet);
        return generatedHdWallet;
    }

    public void persistHDWallet(Coin coin, HDWallet generatedHdWallet) {
        Wallet wallet = new Wallet();
        if (walletRepository.findByCoinAndAddress(coin, generatedHdWallet.getHdWalletAddress().getAddress()).isPresent())
            return;
        wallet.setCoin(coin);
        wallet.setAddress(generatedHdWallet.getHdWalletAddress().getAddress());
        wallet.setPrivateKey(generatedHdWallet.getHdWalletAddress().getPrivateKey());
        wallet.setMnemonic(generatedHdWallet.getCredentials().getMnemonic());
        wallet.setCreatedDate(ZonedDateTime.now());
        this.walletRepository.save(wallet);
    }

    @Override
    public String generateMnemonic() {
        return MnemonicHelper.generateMnemonic();
    }

    @Override
    public WalletBalanceDTO checkBalance(Coin coin, String address) {
        CoinManager manager = coinManagerFactory.getManager(coin);
        BigDecimal macroValue = manager.getAddressBalance(address);
        BigDecimal microValue = coin.convertMacroToMicroValue(macroValue);
        WalletBalanceDTO walletBalanceDTO = new WalletBalanceDTO();
        walletBalanceDTO.setMacro(macroValue);
        walletBalanceDTO.setMicro(microValue);
        return walletBalanceDTO;
    }

    @Override
    public AccountStatus checkFinalBalance(Coin coin, String address) {
        CoinManager manager = coinManagerFactory.getManager(coin);
        return manager.getTransactionStatus(address);
    }

    @Override
    public Map<CryptoCurrencyApiProvider, AccountStatus> checkFinalBalanceMap(Coin coin, String address) {
        CoinManager manager = coinManagerFactory.getManager(coin);
        return manager.getTransactionStatusMap(address);
    }


}
