package ir.kavoshgaran.bitrahcore.services;

import ir.kavoshgaran.bitrahcore.core.hdwallets.HDWallet;
import ir.kavoshgaran.bitrahcore.core.providers.CryptoCurrencyApiProvider;
import ir.kavoshgaran.bitrahcore.core.transactions.services.AccountStatus;
import ir.kavoshgaran.bitrahcore.entities.Coin;
import ir.kavoshgaran.bitrahcore.services.dto.WalletBalanceDTO;

import java.util.Map;

public interface WalletService {

    String generateWallet(Coin coin);

    String generateMnemonic();

    WalletBalanceDTO checkBalance(Coin coin, String address);

    AccountStatus checkFinalBalance(Coin coin, String address);

    Map<CryptoCurrencyApiProvider, AccountStatus> checkFinalBalanceMap(Coin coin, String address);

    HDWallet generateHDWallet(Coin coin, String password);

    HDWallet retrieveHDWallet(Coin coin, String mnemonic, String password);
}
