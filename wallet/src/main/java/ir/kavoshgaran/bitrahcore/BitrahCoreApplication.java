package ir.kavoshgaran.bitrahcore;

import ir.kavoshgaran.bitrahcore.configuration.properties.AuthenticationProperties;
import ir.kavoshgaran.bitrahcore.configuration.properties.CoinTransactionProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
@EnableConfigurationProperties({AuthenticationProperties.class, CoinTransactionProperties.class})
public class BitrahCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(BitrahCoreApplication.class, args);
    }

}
