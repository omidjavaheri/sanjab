package ir.kavoshgaran.bitrahcore.utils;

public class BitrahCheckSumUtils {

    public static byte[] doubleSha256(byte[] input) {
        return BitrahCheckSumUtils.doubleSha256(input, 0, 4);
    }

    public static byte[] doubleSha256(byte[] input, int startRange, int endRange) {
        byte[] secretKeyCheckSum = BitrahHashUtils.doubleSha256Bytes(input);
        return BitrahByteUtils.copyOfRange(secretKeyCheckSum, startRange, endRange);
    }
}
