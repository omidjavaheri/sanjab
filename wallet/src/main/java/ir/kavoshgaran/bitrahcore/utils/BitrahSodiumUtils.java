package ir.kavoshgaran.bitrahcore.utils;

import com.goterl.lazycode.lazysodium.LazySodiumJava;
import com.goterl.lazycode.lazysodium.SodiumJava;
import com.goterl.lazycode.lazysodium.exceptions.SodiumException;
import com.goterl.lazycode.lazysodium.utils.Key;
import com.goterl.lazycode.lazysodium.utils.KeyPair;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyApiException;
import ir.kavoshgaran.bitrahcore.core.transactions.services.CryptoCurrencyError;

public class BitrahSodiumUtils {
    private static final LazySodiumJava lazySodium = new LazySodiumJava(new SodiumJava());

    public static KeyPair generateCryptoSignKeyPair() {
        try {
            return lazySodium.cryptoSignKeypair();
        } catch (SodiumException e) {
            throw new CryptoCurrencyApiException(CryptoCurrencyError.KEY_PAIR_CREATION_ERROR);
        }
    }

    public static KeyPair generateCryptoSignKeyPairFromPrivateKey(String privateKey) {
        try {
            return lazySodium.cryptoSignSecretKeyPair(Key.fromHexString(privateKey));
        } catch (SodiumException e) {
            throw new CryptoCurrencyApiException(CryptoCurrencyError.KEY_PAIR_CREATION_ERROR);
        }
    }

    public static byte[] cryptoGenericHash(int length, byte[] input) {
        byte[] hashBytes = new byte[length];
        lazySodium.cryptoGenericHash(hashBytes, hashBytes.length, input, input.length);
        return hashBytes;
    }

    public static byte[] cryptoSignDetached(int signatureLength, byte[] input, String privateKey) {
        return BitrahSodiumUtils.cryptoSignDetached(signatureLength, input, BitrahHexUtils.decodeHexString(privateKey));
    }

    public static byte[] cryptoSignDetached(int signatureLength, byte[] input, byte[] privateKey) {
        byte[] signature = new byte[signatureLength];
        lazySodium.cryptoSignDetached(signature, input, input.length, privateKey);
        return signature;
    }
}
