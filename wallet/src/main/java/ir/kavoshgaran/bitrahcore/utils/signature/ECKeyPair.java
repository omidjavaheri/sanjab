package ir.kavoshgaran.bitrahcore.utils.signature;

import ir.kavoshgaran.bitrahcore.utils.BitrahHexUtils;

public class ECKeyPair {
    private final byte[] privateKey;
    private final byte[] publicKey;

    public ECKeyPair(byte[] privateKey, byte[] publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public byte[] getPrivateKey() {
        return privateKey;
    }

    public byte[] getPublicKey() {
        return publicKey;
    }

    public String getPrivateKeyHex() {
        return BitrahHexUtils.bytesToHex(privateKey);
    }

    public String getPublicKeyHex() {
        return BitrahHexUtils.bytesToHex(publicKey);
    }

}
