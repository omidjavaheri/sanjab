package ir.kavoshgaran.bitrahcore.utils;

import ir.kavoshgaran.bitrahcore.entities.ErrorDTO;
import ir.kavoshgaran.bitrahcore.entities.WalletResponseDTO;

import java.util.Date;

public class WalletResponseUtils {

    public static <Type> WalletResponseDTO<Type> response(Type response) {
        return response(response, null);
    }

    public static <Type> WalletResponseDTO<Type> response(Type response, String message) {
        WalletResponseDTO result = new WalletResponseDTO();
        result.setData(response);
        result.setMessage(message);
        result.setSuccess(true);
        result.setTimestamp(new Date());
        return result;
    }

    public static WalletResponseDTO<ErrorDTO> error(ErrorDTO errorDTO) {
        WalletResponseDTO result = new WalletResponseDTO();
        result.setData(errorDTO);
        result.setMessage(null);
        result.setSuccess(false);
        result.setTimestamp(new Date());
        return result;
    }
}
