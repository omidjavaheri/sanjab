package ir.kavoshgaran.bitrahcore.utils.serializer;

public abstract class Content {

    abstract byte[] getBytes();
}
